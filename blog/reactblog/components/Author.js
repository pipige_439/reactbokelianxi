import {Avatar,Divider} from 'antd'
import '../static/style/components/author.css'
import { GithubOutlined,RedditOutlined,WifiOutlined } from '@ant-design/icons';

const Author =()=>{

    return (
        <div className="author-div comm-box">
            <div> <Avatar size={100} src="http://img.duoziwang.com/2020/01/05201321823183.jpg"  /></div>
            <div className="author-introduction">
                光头程序员，专注于WEB和移动前端开发。要录1000集免费前端视频的傻X。此地维权无门，此时无能为力，此心随波逐流。
                <Divider>社交账号</Divider>
                <Avatar size={28} icon={<GithubOutlined />} className="account"  />
                <Avatar size={28} icon={<RedditOutlined />}  className="account" />
                <Avatar size={28} icon={<WifiOutlined />}  className="account"  />

            </div>
        </div>
    )

}

export default Author